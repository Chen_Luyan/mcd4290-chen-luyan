//Task 1

  let radius = 4
  let circumference = 2*Math.PI*radius
  let myAnswer1 = circumference.toFixed(2)
  console.log(myAnswer1)

//Task 2

  let animalString = "cameldogcatlizard"
  let andString = "and"
  console.log(animalString.substr(5,3) + ' ' + andString + ' ' + animalString.substr(8,3))

//Task 3

  let person = {
    firstName: "Kanye",
    lastName: "West",
    birthDate: "8 June 1977",
    annualIncome: 150000000.00,
  }
  console.log(person.firstName + ' ' + person.lastName + ' ' + "was born on " + person.birthDate + ' ' + "and has an annual income of " + person.annualIncome)


//Task 4

  var number1, number2;

  number1 = Math.floor((Math.random() * 10) + 1);
  number2 = Math.floor((Math.random() * 10) + 1);

  console.log("number1 = " + number1 + " number2 = " + number2);

  let numberSwap = number1
  number1 = number2
  number2 = numberSwap

  console.log("number1 = " + number1 + " number2 = " + number2)

//Task 5
  
  let year;
  let yearNot2015or2016;
  
  year = 2010;
  yearNot2015or2016 = year !== 2015 && year !==2016;

  console.log(yearNot2015or2016)