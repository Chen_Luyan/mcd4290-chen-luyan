question1();
question2();
question3();
question4();

function question1(){
    
    let output = "";
    var outputAreaRef = document.getElementById("outputArea1");
   
    var testObj = {

        number: 1,

        string: "abc",

        array: [5, 4, 3, 2, 1],

        boolean: true

    };
    output += objectToHTML(testObj)
    outputAreaRef.innerText = output;
    
    function objectToHTML(obj){
        var result = "";
        for (var prop in obj){
            result += prop + ": " + obj[prop] + "\n";
        }
        return result
    }
        
    let outPutArea = document.getElementById("outputArea1") 
    outPutArea.innerText = output 
}

function question2(){
    
    let output = "";
    var outputAreaRef = document.getElementById("outputArea2");


    function flexible(fOperation, operand1, operand2){
        let result = fOperation(operand1, operand2);

        return result;
    }

    function sum(a, b) {
        return a+b;
    }
    
    function multiply(num1, num2) {
        return num1 * num2
    }

    output += flexible(sum, 3, 5) + "\n";
    output += flexible(multiply, 3, 5) + "\n";
        
    let outPutArea = document.getElementById("outputArea2")
    outPutArea.innerText = output
}

function question3(){
    /*
    Create a function called extremeValues and take the given array as the parameter
    Define an array for max and min 
    use a for loop for the array and check through the elements in the array
    if the element is less than the number in the min array then it is set as the new min
    if the element is more than the number in the max array then it is set as the new max 
    loop until all the elements in the give array is done
    print out the results from the min and max
    */    
    
    let output = "Refer to source code for qns 3"
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output
}

function question4(){
    
    var arrayValue = [4, 3, 6, 12, 1, 3, 8];
    extremeValues(arrayValue)
    
    function extremeValues(array) {
        let min = array[0];
        let max = array[0];
        
        for(let i=0; i<array.length; i++){
            if(array[i]<min){
                min = array[i]
            }
            
            if(array[i]>max){
                max=array[i]
            }
        }
        let result = [min,max]
        let outPutArea = document.getElementById("outputArea4") 
        outPutArea.innerText += 
            "Minimum: " + result[0] + "\n" +
            "Maximum: " + result[1]
    }
    
}
