//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    
    let myData = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25] 
    
    let posOdd = []
    
    let negEven = []
    
    for(let i = 0; i < myData.length; i++)
        
        if(myData[i]>0 && myData[i]%2){
            posOdd.push(myData[i])
        }
        else if(myData[i]<0 && myData[i]%2===0){
            negEven.push(myData[i])
        }
    
    let output = ("Positive odd: " + posOdd + "\n" + "Negative even: " + negEven)
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    
    var one = 0
    var two = 0
    var three = 0
    var four = 0
    var five = 0
    var six = 0
    
    for (var i=0;i<60000;i++){

    var dice = Math.floor(Math.random() * 6) + 1

    if (dice==1){
        one+=1
    }
    else if(dice==2){
        two+=1
    }
    else if(dice==3){
        three+=1
    }
    else if(dice==4){
        four+=1
    }
    else if(dice==5){
        five+=1
    }
    else if(dice==6){
        six+=1
    }
    
    }
    
    let output = ("Frequency of die rolls" + "\n" + 
                  "1: "+one + "\n" + 
                  "2: "+two + "\n" +
                  "3: "+three + "\n" +
                  "4: "+four + "\n" + 
                  "5: "+five + "\n" +
                  "6: "+six)
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    
    var diceNum = new Array(6).fill(0);
    var randNum;
    
    for(var i=1; i<=60000; i++){
    randNum = Math.floor(Math.random()*6) + 1;
    diceNum[randNum]++;
    }
    
    let answer = ("Frequency of dice rolls" + "\n")
    
    for (var j=1; j<diceNum.length ; j++){
        answer += j + ":" + diceNum[j] + "\n"
    }
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = answer 
}

function question4(){
    
    var dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions:""
    }
    
    for(let i=1; i<dieRolls.total; i++){
        var randNum = Math.floor(Math.random()*6) + 1;
        dieRolls.Frequencies[randNum]++;
    }
    
    var expectedRolls = 10000
    var onePercent = expectedRolls * 0.01;
    for (j = 1; j<=6; j++){
        var difference = dieRolls.Frequencies[j] - expectedRolls;
        if (Math.abs(difference) > onePercent){
            dieRolls.Exceptions += j + " ";
        }
    }
    
    output = "Frequency of dice rolls" + "\n";
    output += "Total rolls: " + dieRolls.Total + "\n";
    output += "Frequencies: ";
    for(var j in dieRolls.Frequencies){
        output += j + ":" + dieRolls.Frequencies[j];
    }
    output += "\n" + "exceptions: " + dieRolls.Frequencies[j];
            
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output
}

function question5(){
    
    let person = {
        name: "Jane",
        income: 127050
    }
    
    let tax = 0 
    if(person.income<18200){
        tax = 0
    }
    else if(person.income<37000){
        tax = ((person.income - 18200) * 0.19)
    }
    else if(person.income<90000){
        tax = ((person.income - 37000) * 0.325)
    }
    else if(person.income<180000){
        tax = ((person.income - 90000) * 0.37)
    }
    else{
        tax = (54097 + (person.income - 180000) * 0.45 )
    }
    
    output = person.name + "'s income is: " +person.income + ", and her tax owned is:$" +tax 
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}